drop database if exists db_7;
create database db_7;
use db_7;

drop table if exists types;
create table types (
   type_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   type_name ENUM('S', 'B') NOT NULL UNIQUE
);

insert into types (type_name) values ('S'), 
									 ('B');
                                     
drop table if exists instruments;
create table instruments (
   instrument_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   instrument_name VARCHAR(15) NOT NULL UNIQUE
);

insert into instruments (instrument_name) values ("Astronomica"), 
												 ("Borealis"), 
                                                 ("Celestial"), 
                                                 ("Deuteronic"),
                                                 ("Eclipse"),
                                                 ("Floral"),
                                                 ("Galactia"),
                                                 ("Heliosphere"),
                                                 ("Interstella"),
                                                 ("Jupiter"),
                                                 ("Koronis"),
                                                 ("Lunatic");


drop table if exists counter_parties;
create table counter_parties (
   counter_party_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   counter_party_name VARCHAR(10) NOT NULL UNIQUE
);

insert into counter_parties (counter_party_name) values ("Lewis"),
														("Selvyn"),
                                                        ("Richard"),
                                                        ("Lina"),
                                                        ("John"),
                                                        ("Nidia");


drop table if exists deals;
create table deals (
   deal_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   instrument_id INT UNSIGNED NOT NULL,
   counter_party_id INT UNSIGNED NOT NULL,
   price DECIMAL(19,14) UNSIGNED NOT NULL,
   type_id INT UNSIGNED NOT NULL,
   quantity INT UNSIGNED NOT NULL,
   time DATETIME(6) NOT NULL,
   FOREIGN KEY (instrument_id) REFERENCES instruments(instrument_id),
   FOREIGN KEY (counter_party_id) REFERENCES counter_parties(counter_party_id),
   FOREIGN KEY (type_id) REFERENCES types(type_id)
);

SELECT * from deals;

drop table if exists users;
create table users (
   user_id  INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   user_name VARCHAR(20),
   user_login VARCHAR(20) NOT NULL,
   user_password VARCHAR(30) NOT NULL
);

insert into users (user_name, user_login, user_password) values ("Beresneva", "admin", "admin");
select * from users;
